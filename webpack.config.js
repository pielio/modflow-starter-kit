var path = require( 'path' ),
	webpack = require( 'webpack' ),
	HtmlWebpackPlugin = require( 'html-webpack-plugin' );

module.exports = {
	devtool: 'eval-source-map',
	devServer: {
		headers: { "Access-Control-Allow-Origin": "*" }
	},
	entry: [
		'react-hot-loader/patch',
		'webpack-dev-server/client?http://0.0.0.0:3000',
		'webpack/hot/only-dev-server',
		path.join(__dirname, 'src/index.js')
	],
    output: {
      path: path.join(__dirname, 'dist'),
      filename: '[name].js',
      publicPath: '/'
    },
    plugins: [
      new HtmlWebpackPlugin({
            template: 'src/index.tpl.html',
            inject: 'body',
            filename: 'index.html'
          }),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify( 'development' ) })
    ],
    module: {
      loaders: [
      {
        test: /\.worker\.js$/,
        loader: "babel!worker-loader",
        include: path.join(__dirname, 'src')
      },
      {
        test: /\.js$/,
        loader: "file?name=[name].[ext]",
        include: path.join(__dirname, 'src/config')
      },
      {
        test: /\.js$/,
        loader: "file?name=[name].[ext]",
        include: path.join(__dirname, 'libs')
      },
      {
        test: /\.(mp4|png|ogv|gif|webm|jpg)(\?[a-z0-9#=&.]+)?$/,
        loader: "file?name=[name].[ext]",
        include: path.join(__dirname, 'assets')
      },
      {
        test: /\.js$/,
        loaders: [ 'babel-loader' ],
        include: path.join(__dirname, 'src'),
        exclude: path.join(__dirname, 'src/config')
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /\.json$/,
        loader: "json-loader"
      },
      {
        test: /\.less$/,
        loader: "style-loader!css-loader!less-loader"
      },
      {
        test: /\.woff(2)?(\?[a-z0-9#=&.]+)?$/,
        loader: 'url?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.(ttf|eot|svg|png)(\?[a-z0-9#=&.]+)?$/,
        loader: 'file'
      }
      ]
    },
    node: {
      console: 'empty',
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      console: true
    }
  };
