
var webpack = require( 'webpack' ),
	path = require( 'path' ),
	WebpackDevServer = require( 'webpack-dev-server' ),
	watch = require( 'node-watch' ),
	fs = require( 'fs' ),
	spawn = require( 'child-process-promise' ).spawn,
	config = require( './webpack.config' ),
	modflow_config = require( './modflow.config' );


var development_directory = path.join( process.cwd(), modflow_config.module_dev_directory );

if( !fs.existsSync( development_directory ) ) {
	fs.mkdirSync( development_directory );
}

watch( './dev_modules', { recursive: true }, ( evt, name ) => {

	var packg,
		initial_dir,
		packg_loc,
		compiling;

	if( compiling ){
		console.log( '---  ALREADY COMPILING, SAVE AGAIN WHEN COMPLETE ---' );
		return;
	}

	if( name.indexOf( '/src/' ) === -1 ) return;

	compiling = true;
	packg = name.split('/')[ 1 ];
	initial_dir = process.cwd();
	packg_loc = process.cwd() + '/dev_modules/' + packg;

	console.log( 'Compiling module ' + packg );
	process.chdir( packg_loc );
	shellcmd = spawn( 'npm', [ 'run', 'compile' ], { stdio: 'inherit' });
	process.chdir( initial_dir );

	shellcmd.then( ( ) => {
		compiling = false;
		console.log( '--- success : Compiled module ' + packg );
	})

});

new WebpackDevServer(webpack(config), {

	publicPath: config.output.publicPath,
	hot: true,
	historyApiFallback: true,

	// It suppress error shown in console, so it has to be set to false.
	quiet: false,

	// It suppress everything except error, so it has to be set to false as well
	// to see success build.
	noInfo: false,
	stats: {

		// Config for minimal console.log mess.
		assets: false,
		colors: true,
		version: false,
		hash: false,
		timings: false,
		chunks: false,
		chunkModules: false
	}
})
.listen(3000, '0.0.0.0', function (err) {

	if (err) {
		console.log(err);
	}

	console.log('Listening at 0.0.0.0:3000');
});
