import { applyMiddleware, createStore } from 'redux';

import { createLogger } from 'redux-logger';
import promise from 'redux-promise-middleware';
import { createEpicMiddleware } from 'redux-observable';

import reducers from './reducers';
import epics from './epics';

const epicMiddleware = createEpicMiddleware( epics );
const middleware = applyMiddleware( createLogger(), promise(), epicMiddleware );

export default function configureStore( initialState ) {
	const store = createStore( reducers, middleware );
	return store;
}
