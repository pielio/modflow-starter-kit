
export default class Actionbase{

  constructor( props ) {

    window.__oidcnt = window.__oidcnt || 9999;
  }

  oid() {

    return ++window.__oidcnt;
  }

  spawn( obj ) {

    obj._oid = ++window.__oidcnt;
    return obj;
  }

  /**

  **/
  seekOne( obj, type, attributes ) {

    var ret, subjectCollection, subject, hit;

    Object.keys( obj ).some( ( key ) => {

      // is this the droid we're looking for?
      if( key === type ) {

        subjectCollection = obj[ key ];

        subjectCollection.some( ( subject ) => {

          hit = Object.keys( attributes ).reduce( ( c, attr ) => {

            if( !c ) return c;
            return ( attributes[ attr ] === subject[ attr ] );

          }, true );

          ret = hit ? subject : false;
          return hit;

        });

      }

      if( ret ) return ret;

      if( obj[ key ] instanceof Array ) {

        obj[ key ].map( ( nested ) => {
         return this.seekOne( nested, type, attributes );
        }).some( ( ret ) => { return ret; });

      }

    });

    return ret;
  }

  seek( args ) {

    var ret = Object.keys( obj ).forEach( ( key ) => {

    });
  }

  /*

    Produce an immutable clone of an argued object which is simply the object parameters , and not nested/related arrays.

  */
  isolate( obj ) {

    return Object.keys( obj ).reduce( ( c, key ) => {
      if( obj[ key ] instanceof Array ) return c;
      c[ key ] = obj[ key ];
      return c;
    }, {});

  }

}
