import ActionBase from './actionbase';
import { withConfig } from '../decorators/configuration';

class YourActions extends ActionBase {

	constructor( props ) {

		super( props );
	}

	someTestMethod() {

	}
}

export default new YourActions();
