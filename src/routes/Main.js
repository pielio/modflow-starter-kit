//import '../style/master.less';
import '../style/main.less';

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Match, Route, withRouter } from 'react-router';
import Root from './Root';

import { withConfiguration } from '../decorators/configuration';

import Header from '../components/Header';
import YourActions from '../actions/YourActions';

@withConfiguration
@withRouter
@connect( ( store ) => { return { store: store } } )
export default class Main extends Component {

	constructor( props ) {

		super( props );
	}

	componentWillMount() {

		this.loadTheme();
	}

	render() {

		return (

			<div className="surface" ref="surface">
				<Header />
				<Route exact path="/" component={ Root } />
			</div>
		);
	}

	loadTheme() {

		if( this.props.config && this.props.config.style && this.props.config.style.theme )
			require( '../style/themes/' + this.props.config.style.theme + '/main.less' );
	}

}
