import React, { Component } from 'react';

export default class ComponentBase extends Component {

	constructor( props ) {

		super( props );
		this.bindMethods();
		this.setInitialState();
	}

	setInitialState(){ }
	bindMethods(){ }
}
