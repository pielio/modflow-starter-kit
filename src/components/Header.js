import '../style/Header.less';

import React from 'react';
import ComponentBase from './ComponentBase';
import { Link } from 'react-router-dom';

export default class Header extends ComponentBase {

	render() {

		return (

			<div className="header">
				<b>...Header...</b>
		 	</div>
		);
	}
}
