import React from 'react';
import { render } from 'react-dom';

import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import configureStore from './store';
import Main from './routes/Main';

const store = configureStore();

render(
	( <AppContainer>
		<Provider store={ store }>
			<Router>
				<Main />
			</Router>
		</Provider>
	</AppContainer> )
	,document.getElementById( 'root' )
)

if( module.hot ) {

	module.hot.accept('./routes/Main', () => {

		const NextMain = require('./routes/Main').default;

		render(
			<AppContainer>
				<Provider store={ store }>
					<Router>
						<NextMain theme={ config.style.theme } />
					</Router>
				</Provider>
			</AppContainer>,
			document.getElementById( 'root' )
		);
	});
}
