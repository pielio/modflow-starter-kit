if( process.env.PRODUCTION === true )
	module.exports = require( './container_prod' );
else
	module.exports = require( './container_dev' );
