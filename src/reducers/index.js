import { combineReducers } from 'redux';

import YourReducers from './YourReducers';

export default combineReducers({
	your_reducers : new YourReducers().respond
});
