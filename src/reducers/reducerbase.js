import CJ from 'circular-json';

export default class reducerbase {

  constructor( ) {

    this.bindMethods();
    window.__oidcnt = window.__oidcnt || 9999;
  }

  bindMethods() {

    this.respond = this.respond.bind( this );
  }

  respond( state, action ) {

    //flick off initial actions redux actions.
    if( action.type === '@@redux/INIT' ) return this.getInitialState();
    if( action.type.indexOf( '@@redux' ) !== -1 ) return {};

    if( typeof this[ action.type ] === 'function' ) {

      var { state, mutants } = this._detachMutables( state );
      state = CJ.parse( CJ.stringify( state ) );
      state = this._attachMutables( state, mutants );

      return this[ action.type ]( state, action );
    }

    return { ...state };
  }

  oid() {

    return ++window.__oidcnt;
  }

  spawn( obj ) {

    obj._oid = ++window.__oidcnt;
    return obj;
  }

  /**

    searches a map for items of specific type, collapsing them to a singular array, returning { <attribute>: [ found, tiems ]} .
    attribute: the desired type
    object : the nested structure

    consider this structure
    {
      roles: [
        {
          name: 'administrative'
          rights:[{
            name: 'admin_domain',
            routes: [ { name 'administrate your domain', url: 'admin-domain'} ]
          },{
            name: 'admin-members',
            routes: [ { name: 'administrate yourmembers', url: 'admin-members '} ]
        }
        ....

    this method will return { routes: [ ... ] };
    in otherwords it scans the entire object tree, searching for arrays of the specified attribute, collapsing them to single array and returning and object as { yourattribute: [ youritems ] }
  **/
  seek( attribute, object, itr ) {

    if( !itr ) object = JSON.parse( CJ.stringify( object ) );

    return Object.keys( object ).reduce( ( c, key ) => {

      if( !c[ attribute ] ) c[ attribute ] = [];
      if( !( object[ key ] instanceof Array ) ) return c;

      if( key === attribute ) c[ attribute ] = c[ attribute ].concat( object[ key ] );

      object[ key ].forEach( ( o, i ) => {

        this.seek( attribute, o, true )[ attribute ].forEach( ( iObj ) => {
          var has = c[ attribute ].some( ( eObj ) => {
            return this._softEqual( eObj, iObj );
          })
          if( !has ) c[ attribute ].push( iObj );
        });

      })
      return c;
    }, {} );

  }

  /**
    Check to see if 2 objects are the same, yet different instances.
  **/
  _softEqual( obj1, obj2 ) {

    return !Object.keys( obj1 ).some( ( key ) => {
      if( ( obj2[ key ] === undefined ) || ( obj2[ key ] !== obj1[ key ] ) ) return true;
    });
  }

  _isAlias( element ) {

  }

  _detachMutables( obj, itr, nest, ret ) {

    itr = itr === undefined ? false : itr;
    nest = nest === undefined ? '' : nest;
    ret = ret === undefined ? { state: obj, mutants: [] } : ret;


    if( Object.keys( obj ).indexOf( '_forceMutable') !== -1 ) {
      ret.mutants.push({ nest: nest, mutant : obj });
      return ret;
    }

    var out = Object.keys( obj ).reduce( ( c, key ) => {

      if( obj[ key ] instanceof Array ) {

        obj[ key ].forEach( ( subobj, i ) => {

          if( !this._isRealObject( subobj) ) return;
          var nested = nest.length ? nest + '.' + key : key;
          nested += '.' + i;
          var result = this._detachMutables( subobj, true, nested, ret );

          c.mutants = result.mutants.reduce( ( cc, mutant ) => {
            if( cc.indexOf( mutant ) === -1 ) cc.push( mutant );
            return cc;
          }, c.mutants);
        });

      }

      if( this._isRealObject( obj[ key ] ) ){

        var nested =  nest.length ? nest + '.' + key : key;

        var result = this._detachMutables( obj[ key ], true, nested, ret );
        c.mutants = result.mutants.reduce( ( cc, mutant ) => {
          if( cc.indexOf( mutant ) === -1 ) cc.push( mutant );
          return cc;
        }, c.mutants);
      }

      c.state[ key ] = obj[ key ];
      return c;
    }, {
      state: {},
      mutants: []
    })

    if( !itr ) {

      out.mutants.forEach( ( mutant ) => {

        var nparent, key;

        nparent = mutant.nest.split('.').slice( 0, -1 ).reduce((o,i)=>o[i], out.state );
        if( nparent instanceof Array ) {

          key = parseInt( mutant.nest.split('.').slice( -1 )[ 0 ], 10 );
          delete nparent[ key ];
          nparent.length--;
        }
        else if( this._isRealObject( nparent ) ) {
          key = parseInt( mutant.nest.split('.').slice( -1 )[ 0 ], 10 );
          delete nparent[ key ];
        }
        else {
          console.log( "DEV DEBUG ( _detachMutables ) - not array or object" );
        }

      })

    }

    return out;
  }

  _attachMutables( obj, mutables ) {

    mutables.forEach( ( mutant ) => {

      var nparent, key;

      nparent = mutant.nest.split('.').slice( 0, -1 ).reduce((o,i)=>o[i], obj );

      if( nparent instanceof Array ) {

        key = parseInt( mutant.nest.split('.').slice( -1 )[ 0 ], 10 );
        nparent[ key ] = mutant.mutant;
      }
      else if( this._isRealObject( nparent ) ) {

        key = parseInt( mutant.nest.split('.').slice( -1 )[ 0 ], 10 );
        nparent[ key ] = mutant.mutant;
      }
      else {

        console.log( "DEV DEBUG ( _attachMutables ) - not array or object" );
      }

    })

    return obj;
  }

  _isRealObject( obj ) {

    return ( !!obj ) && ( obj.constructor === Object );
  }

}
