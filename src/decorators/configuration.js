import config from '../config.json';
import React from 'react';

window.config = config;

export const withConfig = function( target ) {
	target.prototype.config = config;
}

export const withConfiguration = function( DecoratedComponent ) {

	return function( props, name, descriptor ) {

		var newProps = JSON.parse( JSON.stringify( props ) );
		newProps.config = config;
		return ( <DecoratedComponent { ...newProps } /> );
	}
}
